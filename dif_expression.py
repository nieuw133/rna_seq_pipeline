"""
Author: Ronald Nieuwenhuis

Simple function to run

"""
import subprocess as sp
import sys


def run_deseq2_r(rscript_exe, deseq_scipt, r_deseq_dir, ref_treatment_name, treatments, csv_file, min_count=1,
                 p_val=0.05):
    """ Run desq2 r-script externally

    csv_file: str, filename (absolute path) to csv counts file
    """
    cmd = f"{rscript_exe} {deseq_scipt} {csv_file} {min_count} {p_val} {treatments} {r_deseq_dir} {ref_treatment_name}"
    print(cmd)
    try:
        process = sp.check_output(cmd.split())
        print(process.decode())
    except sp.CalledProcessError as CPE:
        print(f"Your command: {cmd} failed with returncode {CPE.returncode} and the following message:\n{CPE.stderr}")
        sys.exit(105)


if __name__ == "__main__":
    print("Nothing to run")
