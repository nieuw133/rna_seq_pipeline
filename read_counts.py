#!/usr/bin/env python3
"""
Author: Alina Iurco

Do read counting using stringtie and convert gtf output to csv using prepDE.py
"""
import sys
import subprocess as sp
import os.path


def run_stringtie(stringtie_exe, bam_file, annotation_file, output_fn, threads=1):
    """ Run stringtie on bam_file, store output in output_fn

    bam_file: name of the input file in BAM format
    annotation_file: name of the input file in GFF format

    If the output_fn already exists, the command will not be executed

    """
    if os.path.isfile(output_fn):
        return output_fn
    cmd = f"{stringtie_exe} {bam_file} -e -p {threads} -G "\
        f"{annotation_file} -o {output_fn}"
    try:
        sp.check_call(cmd.split())
    except sp.CalledProcessError as CPE:
        print(f"Your command: {cmd} failed with exitcode {CPE.returncode}")
        sys.exit(90)
    return output_fn


def run_prepDE(python2_exe, prepde_location, sample_description_file, output_fn):
    """ Run prepDE.py on .txt file

    If the output_fn doesn't already exist, the command will not be executed
    """
    if os.path.exists(output_fn):
        return output_fn
    cmd = f"{python2_exe} {prepde_location} --input={sample_description_file}" \
          f" -g {output_fn}"
    try:
        sp.check_call(cmd.split())
    except sp.CalledProcessError as CPE:
        print(f"Your command: {cmd} failed with exitcode {CPE.returncode}")
        sys.exit(91)
    return output_fn


def prep_sample_description(samples_dict, output_fn):
    """ Prep sample description file, it is a tab separated file with sample
        name and absolute path to gtf file that contains counts

    samples_dict: dict, key is sample name value is absolute path to gtf file

    output_fb: str, filename (absolute path) to output tsv file

    NOTE: will overwrite file if it exists
    """
    with open(output_fn, mode="w") as open_file:
        for key, value in samples_dict.items():
            open_file.write(f"{key}\t{value}\n")
    return output_fn


if __name__ == "__main__":
    bam_file = argv[1]
    gff_file = argv[2]

    output_fn = 'test_stringtie.gtf'
    read_counts = 'read_counts.gff'

    run_stringtie()
    run_prepDE()
