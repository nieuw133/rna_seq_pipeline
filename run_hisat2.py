"""
Author: Ronald Nieuwenhuis

Functionality to build a hisat2 index from a reference genome and map reads to this index
"""
import subprocess as sp
import os, sys


def build_hisat2_index(hisat2_exe, reference_fasta, index_name="Hisat2_index", threads=1):
    """Build a reference if it does not yet exist of is incomplete,
    """
    if check_if_complete_hisat2_index(index_name):
        return index_name
    if not os.path.exists(reference_fasta):
        print(f"Reference file {reference_fasta} does not exist. Exiting")
        sys.exit(97)
    cmd = f"{hisat2_exe}-build -p {threads} {reference_fasta} {index_name}"
    try:
        sp.check_call(cmd.split())
    except sp.CalledProcessError as CPE:
        print(f"{hisat2_exe}-build failed with exitcode {CPE.returncode}")
        sys.exit(98)
    return index_name


def check_if_complete_hisat2_index(index_name):
    """

    :param index_name:
    :return:
    """
    # These are the file suffixes generated when creating a hisat2 index
    complete_suffixes = [".1.ht2", ".2.ht2", ".3.ht2", ".4.ht2", ".5.ht2", ".6.ht2", ".7.ht2", ".8.ht2"]
    check_marks = [True if os.path.isfile(index_name + suffix) else False for suffix in complete_suffixes]
    return all(check_marks)


def run_hisat2(hisat2_exe, output_name, forward_reads, reverse_reads, index_name="Hisat2_index", threads=1):
    """

    :return:
    """
    if os.path.exists(output_name):
        return output_name
    cmd = f"{hisat2_exe} -x {index_name} -p {threads} --dta -1 {forward_reads} -2 {reverse_reads} " \
          f"-S {output_name}"
    try:
        sp.check_call(cmd.split())
    except sp.CalledProcessError as CPE:
        print(f"Your command {cmd} failed with exitcode {CPE.returncode}")
        sys.exit(101)
    return output_name


if __name__ == "__main__":
    print("Nothing to run")