"""
Author: Ronald Nieuwenhuis

General parameter class that can parse config files and can have a default config
"""
import os
from datetime import date
import yaml


class Parameters:
    """ Workflow object is basically a namespace object that contains the
    parameters needed for the pipeline and defaults if they are not specified
    in the config file
    """
    def __init__(self, config_file, defaults_config=None):
        if not os.path.isfile(config_file):
            print(f"Config file: {config_file} does not exist!")
            sys.exit(92)
        # A defaults file makes this more modular and the following attribute assignments unneccesary..
        self.working_directory=os.getcwd()
        self.workflow_name = f"RNA_seq_pipeline_{date.today().strftime('%b-%d-%Y')}"
        self.threads = 1
        if defaults_config:
            self._set_defaults(defaults_config)
        self.parsed_yaml = self._parse_config_file(config_file)
        self._set_variables(self.parsed_yaml)

    def _set_defaults(self, defaults_config):
        """Set default values for all attributes

        :param defaults_config:
        :return:
        """
        parsed_default = self._parse_config_file(defaults_config)
        self._set_variables(parsed_default)

    def _parse_config_file(self, config_file):
        """ Parse the config file and set parameters
        """
        with open(config_file) as open_file:
            try:
                parsed_yaml = yaml.safe_load(open_file)
            except yaml.YAMLError as YE:
                print(f"Error during parsing of YAML config file {config_file}")
                if hasattr(YE, 'problem_mark'):
                    mark = YE.problem_mark
                    print(f"Error position: ({mark.line + 1}:{mark.column + 1})")
                sys.exit(93)
        return parsed_yaml

    def _set_variables(self, parsed_yaml):
        """Set object attributes, recursive for nested dictionaries

        :param parsed_yaml:

        """
        for key, value in parsed_yaml.items():
            if isinstance(value, dict):
                self._set_variables(value)
            else:
                setattr(self, key, value)

    def __str__(self):
        """Print the objects namespace

        :return: str, newline separated attributes tab delimited from their values
        """
        return '\n'.join([f"{key}\t{value}" for key, value in self.__dict__.items() if not key == "parsed_yaml"])
