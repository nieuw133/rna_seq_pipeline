#!/usr/bin/env python3
"""
Author: Ronald Nieuwenhuis
RNA-seq main pipeline and 2 input checks on the input data and parameters.
"""
import sys, os
from parameters import Parameters
from run_hisat2 import build_hisat2_index, run_hisat2
from convert_sam_to_bam import convert_sam_to_bam
from read_counts import run_stringtie, run_prepDE, prep_sample_description
from dif_expression import run_deseq2_r


def test_parameter_completeness(parameters):
    """Test whether the parameters object contains the minimal needed arguments to run this pipeline

    :return:
    """
    minimal_needed = {"hisat2_executable": str(),
                      "stringtie_executable": str(),
                      "rscript_executable": str(),
                      "samtools_executable": str(),
                      "prepDE_script_location": str(),
                      "python2_executable": str(),
                      "deseq2_r_script": str(),
                      "working_directory": str(),
                      "workflow_name": str(),
                      "threads": int(),
                      "control": str(),
                      "sample_1": str(),
                      "control_forward": list(),
                      "control_reverse": list(),
                      "sample_1_forward": list(),
                      "sample_1_reverse": list(),
                      "reads_directory": str(),
                      "index": str(),
                      "reference_fasta": str(),
                      "p_val": float(),
                      "minimum_count": int(),
                      "replicates": list()
                      }

    # Check if minimal requirements are met and values are of good type_
    for key, value in minimal_needed.items():
        try:
            attribute = getattr(parameters, key)
        except AttributeError as AE:
            print(f"The parameter {key} has not been specified. Exiting.")
            sys.exit(94)
        if not isinstance(attribute, type(value)):
            print(f"Parameter {key} must be of type {value}, is {type(attribute)} instead.")
            sys.exit(96)
    return parameters


def group_inputs(parameter_object):
    """

    :param parameter_object:
    :return:
    """
    prmt_namespace = vars(parameter_object)

    # Get number of 'sample' occurrences in parameter configuration, should be multiple of 3
    # this will fail if sample numbering is not consecutively numbered starting with 1
    number_of_samples = sum([True for key in sorted(prmt_namespace.keys()) if 'sample' in key]) / 3
    if not number_of_samples.is_integer():
        print("Missing sample parameters. Please check if for each sample_x there is a sample_x_forward and "
              "sample_x_reverse list")
        sys.exit(99)

    replicates = parameter_object.replicates
    if len(parameter_object.replicates) != number_of_samples + 1:
        print("Length of reference parameter is not equal to number of samples, this means all samples are considered"
              " as separate treatments. Treatments are translated in to 1..2..3...#number of samples.")
        replicates = [i for i in range(int(number_of_samples) + 1)]

    # Do the mapping for all samples to the reference index
    # We always have a control sample and sample_1
    control = (parameter_object.control, parameter_object.control_forward, parameter_object.control_reverse,
               replicates[0])

    groups = [control]
    for i in range(1, int(number_of_samples + 1)):
        try:
            # The last element is the treatment level as given in the config
            groups.append((prmt_namespace[f"sample_{i}"], prmt_namespace[f"sample_{i}_forward"],
                           prmt_namespace[f"sample_{i}_reverse"], replicates[i]))
        except KeyError as KE:
            print(f"The numbering of your samples is not consecutive. Make sure it starts at 1 and is consecutive."
                  f"When trying to group the sample names with their read files we could nto find key {KE.args[0]} .")
            sys.exit(100)
    if len(set([group[0]for group in groups])) != len(groups):
        print("Sample (& control) names are not unique!")
        sys.exit(101)
    print(groups)
    setattr(parameter_object, "groups", groups)
    return parameter_object


def pipeline(prmt):
    """ Run the pipeline using a Workflow object that describes the parameters


    """
    # Check presence of necessary input parameters and defaults
    minimal_prmt_ok = test_parameter_completeness(prmt)

    # Check if sample parameters are OK and group them in a new attribute
    sample_prmt_added = group_inputs(minimal_prmt_ok)

    prmt = sample_prmt_added
    workflow_instance_dir = prmt.working_directory + '/' + prmt.workflow_name + '/'
    # Create the working directory if it does not exist yet
    try:
        os.mkdir(workflow_instance_dir)
    except FileExistsError as FEE:
        print(f"Working directory {workflow_instance_dir} already exists")

    # create index of reference if it does not exist yet
    if not os.path.isabs(prmt.index):
        setattr(prmt, "index", workflow_instance_dir + prmt.index)
    print(prmt.index)
    index = build_hisat2_index(prmt.hisat2_executable, prmt.reference_fasta,
                               prmt.index, prmt.threads)

    # This is save it will always override it because it has either been created or is existing completely
    setattr(prmt, "index", index)

    # Do the mapping for each sample
    sam_files = [run_hisat2(prmt.hisat2_executable,
                            workflow_instance_dir + sample[0] + '.sam',
                            ",".join([prmt.reads_directory + "/" + fw for fw in sample[1]]),
                            ",".join([prmt.reads_directory + "/" + rv for rv in sample[2]]),
                            prmt.index,
                            prmt.threads)
                 for sample in prmt.groups]

    # Convert the sam file to bam files
    bam_files = [convert_sam_to_bam(prmt.samtools_executable, sam, sam.split('.sam')[0] + "_sorted.bam", prmt.threads)
                 for sam in sam_files]

    # Convert all sams to empty files after they are converted, to save  space
    for sam in sam_files:
        with open(sam, mode='w') as open_file:
            open_file.write('')

    # Do read counting using stringtie
    sample_counts = {os.path.basename(bam.split("_sorted.bam")[0]): run_stringtie(prmt.stringtie_executable,
                                                                                  bam,
                                                                                  prmt.annotation_file,
                                                                                  bam.split("_sorted.bam")[0] +
                                                                                  "_counts.gtf", prmt.threads)
                     for bam in bam_files}

    # Create a file that describes samples and paths to gtf files that contain counts
    # this is needed as input for prepDE.py?
    sample_and_counts_paths_description = prep_sample_description(sample_counts, workflow_instance_dir +
                                                                  "Samples_and_paths_to_gtf_files.txt")
    print(sample_and_counts_paths_description)
    # Use prepDE.py to create a csv counts file
    csv_counts = run_prepDE(prmt.python2_executable, prmt.prepDE_script_location, sample_and_counts_paths_description,
                            sample_and_counts_paths_description.split("Samples_and_paths_to_gtf_files.txt")[0] +
                            f"{prmt.workflow_name}_counts.csv")

    # Get the treatments and hand them over the command line to Rscript as comma-separated string
    treatments = ",".join([str(group[3]) for group in prmt.groups])

    # The reference treatment used by DESeq2 is always the treatment name of the control sample
    ref_treatment_name = prmt.groups[0][3]

    # Create extra directory for the R output
    r_deseq_dir = workflow_instance_dir + '/Differential_expression/'
    r_pathway_dir = workflow_instance_dir + '/Pathway_enrichment/'
    try:
        os.mkdir(r_deseq_dir)
        os.mkdir(r_pathway_dir)
    except FileExistsError as FEE:
        print(f"Working directory {FEE.args} already exists")

    # Run deseq2 script on counts data and do DEG analysis
    run_deseq2_r(prmt.rscript_executable, prmt.deseq2_r_script, r_deseq_dir, ref_treatment_name, treatments, csv_counts,
                 prmt.minimum_count, prmt.p_val)

    print("End of pipeline")


def main():
    # Config file input is mandatory
    config_file = sys.argv[1]
    default = "/home/nieuw133/Advanced_bioinformatics/Project/tomatonators/Pipeline/" \
              "Ronalds_view_on_a_pipeline/Defaults.cfg"
    try:
        default = sys.argv[2]
    except IndexError:
        if not os.path.isfile(default):
            sys.exit(3)

    workflow_parameters = Parameters(config_file, default)

    pipeline(workflow_parameters)


if __name__ == "__main__":
    main()
