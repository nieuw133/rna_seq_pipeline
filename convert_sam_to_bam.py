"""
"""
import subprocess as sp
import os, sys


def convert_sam_to_bam(samtools_exe, sam_in, bam_out, threads=1):
    """

    """
    if os.path.exists(bam_out):
        return bam_out
    cmd = f"{samtools_exe} sort -@ {threads} -o {bam_out} {sam_in}"
    try:
        sp.check_call(cmd.split())
    except sp.CalledProcessError as CPE:
        print(f"Your command {cmd} failed with exitcode {CPE.returncode}")
        sys.exit(102)
    return bam_out
